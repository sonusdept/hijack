Simple bridge that creates input and output ports to connect VCV Rack to JACK.

To use it, start Rack, start JACK, then start this bridge.
On Rack, choose "Bridge -> Port 1" as audio interface.
Done, you will see the JACK ports.

To build it, clone this repository inside your Rack (source + build) folder,
then cd to hijack and run make.
