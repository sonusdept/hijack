#include <stdlib.h>
#include <stdio.h>
#include <math.h>


#include "audioio.hpp"

BridgeClient *bridge_client;
AudioIO *jack_client;

int main(int argc, char *argv[])
{
    jack_client = new AudioIO();
    jack_client->instantiate("Hijack", BRIDGE_INPUTS, BRIDGE_OUTPUTS, "In", "Out");

    bridge_client = new BridgeClient();

    char a;
    std::cout << "Press any key + [Enter] to exit...\n";
    std::cin >> a;

    return 0;
}