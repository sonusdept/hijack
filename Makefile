VERSION = master

RACK_DIR ?= ..
include $(RACK_DIR)/arch.mk

ifeq ($(ARCH), mac)
	$(error Hijack is currently Linux only)
endif
ifeq ($(ARCH), win)
	$(error Hijack is currently Linux only)
endif
ifeq ($(ARCH), lin)
	FLAGS += -ljack -lasound -lpthread -O2 -fomit-frame-pointer -pipe -Wall
endif

SOURCES += *.cpp

all:
	g++ $(SOURCES) $(FLAGS) -I$(RACK_DIR)/include -o hijack

clean:
	rm -rfv hijack

