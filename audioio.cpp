#include "audioio.hpp"

typedef jack_default_audio_sample_t sample_t;


AudioIO::AudioIO()
{

}

AudioIO::~AudioIO()
{

}

int AudioIO::audio_process(jack_nframes_t nframes)
{
    std::vector<sample_t*> in;
    std::vector<sample_t*> out;

    for (unsigned int p = 0; p < BRIDGE_INPUTS; p++)
    {
        in.push_back((sample_t *)jack_port_get_buffer(input_ports[p], nframes));
    }

    for (unsigned int p = 0; p < BRIDGE_OUTPUTS; p++)
    {
        out.push_back((sample_t *)jack_port_get_buffer(output_ports[p], nframes));
    }

    for (jack_nframes_t s = 0; s < nframes; s++)
    {
        float input[BRIDGE_INPUTS * nframes];
		float output[BRIDGE_OUTPUTS * nframes];

		for (unsigned int i = 0; i < nframes; i++)
        {
			for (int c = 0; c < BRIDGE_INPUTS; c++)
            {
				input[BRIDGE_INPUTS*i + c] = in.at(c)[i];
			}
		}

        bridge_client->processStream(input, output, nframes);

        for (unsigned int o = 0; o < nframes; o++)
        {
			for (int c = 0; c < BRIDGE_OUTPUTS; c++)
            {
				out.at(c)[o] = output[BRIDGE_OUTPUTS*o + c];
			}
		}
    }

    return 0;
}