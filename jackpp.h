#ifndef JACKPP_H_
#define JACKPP_H_

#include <iostream>
#include <thread>
#include <vector>

#include <jack/jack.h>

#include "client.hpp"


int process(jack_nframes_t nframes, void *arg);

class JackPP
{
public:
    virtual ~JackPP()
    {
        jack_client_close(client);
    }

    static void jack_shutdown(void *arg)
    {
        std::cerr << "JACK server stopped\n";
    }

    virtual int audio_process(jack_nframes_t nframes)
    {
        return 0;
    }

    bool instantiate(const char *client_name, unsigned int in_channels,
                     unsigned int out_channels, const char *input_prefix = "input",
                     const char *output_prefix = "output");

    float get_sample_rate();

protected:
    std::vector<jack_port_t*> input_ports;
    std::vector<jack_port_t*> output_ports;
    jack_client_t *client;
    float sample_rate;
};

#endif // JACKPP_H_
