#ifndef AUDIOIO_H_
#define AUDIOIO_H_

#include "jackpp.h"

class AudioIO : public JackPP
{
public:
    AudioIO();
    ~AudioIO();

    int audio_process(jack_nframes_t nframes);
};

extern AudioIO *jack_client;
extern BridgeClient *bridge_client;

#endif // AUDIOMIDI_H_
